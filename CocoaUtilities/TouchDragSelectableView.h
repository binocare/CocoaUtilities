//
//  TouchDragSelectableView.h
//  OpenCVCameraPipeline
//
//  Created by JiangZhiping on 16/4/8.
//  Copyright © 2016年 JiangZhiping. All rights reserved.
//

#import "TouchEnabledNSUIView.h"

@interface TouchDragSelectableView : TouchEnabledNSUIView

@end

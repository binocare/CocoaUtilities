//
//  RegionSelectableView.h
//  OpenCVCameraPipeline
//
//  Created by JiangZhiping on 16/4/8.
//  Copyright © 2016年 JiangZhiping. All rights reserved.
//

#import <CocoaXtensions/CocoaXtensions.h>
#import <CoreGraphics/CoreGraphics.h>
/**
 *  The root class for MouseDragSelectableView and
 *  TouchDragSelectableView.
 *
 * @ref TouchEnabledNSUIView
 */
@interface TouchEnabledNSUIView : NSUIView

@property (nonatomic) BOOL touchEnabled;

@property (nonatomic) CGRect selectedRect;

@property (nonatomic) CGPoint hitPoint;

@end

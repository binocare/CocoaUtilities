//
//  FPSTimer.m
//  GazeEstimationOnCocoa
//
//  Created by JiangZhiping on 15/11/19.
//  Copyright © 2015年 JiangZhiping. All rights reserved.
//

#import "TickTockTimer.h"

@implementation TickTockTimer {
    NSMutableArray * durationArray;
    NSDate * startTime;
    NSString * timerName;
    NSInteger length;
}

-(instancetype) initWithName:(NSString *)aName AverageLength:(NSInteger)anInteger {
    self = [super init];
    if (self) {
        timerName = aName;
        length = anInteger;
        durationArray = [[NSMutableArray alloc] initWithCapacity:anInteger];
    }
    
    return self;
}

- (void) tick {
    startTime = [NSDate date];
}

- (void) tock {
    NSTimeInterval duration = -startTime.timeIntervalSinceNow;
    [durationArray addObject:@(duration)];
    if (durationArray.count > length) {
        [durationArray removeObjectAtIndex:0];
    }
    NSNumber * averageDuration = [durationArray valueForKeyPath:@"@avg.doubleValue"];
    NSLog(@"Timer#%@# %.3fs %0.1ffps",timerName, averageDuration.floatValue, 1.0/averageDuration.floatValue);
}

@end

//
//  MouseDragView.h
//  VADSIntegrated
//
//  Created by JiangZhiping on 16/4/7.
//  Copyright © 2016年 JiangZhiping. All rights reserved.
//

#import <Cocoa/Cocoa.h>
#import <QuartzCore/QuartzCore.h>
#import "TouchEnabledNSUIView.h"

/**
 *  This class add "drag-draw rectangle" effect to the view.
 *  When the selection is done, the instance of this class will
 *  send an "MouseDragSelectionDone" notification through NSNotification center.  The attached notifaction.object will be a NSValue of CGRect.
 */
@interface MouseDragSelectableView : TouchEnabledNSUIView

@end

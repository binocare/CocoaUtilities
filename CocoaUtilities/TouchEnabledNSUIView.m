//
//  RegionSelectableView.m
//  OpenCVCameraPipeline
//
//  Created by JiangZhiping on 16/4/8.
//  Copyright © 2016年 JiangZhiping. All rights reserved.
//

#import "TouchEnabledNSUIView.h"


@implementation TouchEnabledNSUIView

- (instancetype) initWithFrame:(CGRect)frame {
    self = [super initWithFrame:frame];
    if (self) {
        self.touchEnabled = YES;
    }
    return self;
}

- (instancetype) initWithCoder:(NSCoder *)aDecoder {
    self = [super initWithCoder:aDecoder];
    if (self) {
        self.touchEnabled = YES;
    }
    return self;
}




@end

//
//  TouchDragtouchEnabledView.m
//  OpenCVCameraPipeline
//
//  Created by JiangZhiping on 16/4/8.
//  Copyright © 2016年 JiangZhiping. All rights reserved.
//

#import "TouchDragSelectableView.h"

@interface TouchDragSelectableView ()

@property (nonatomic, strong) CAShapeLayer *shapeLayer;

@property (nonatomic) CGPoint startPoint;
@property (nonatomic) CGPoint endPoint;

@end

@implementation TouchDragSelectableView

/*
// Only override drawRect: if you perform custom drawing.
// An empty implementation adversely affects performance during animation.
- (void)drawRect:(CGRect)rect {
    // Drawing code
}
*/



- (void) touchesBegan:(NSSet<UITouch *> *)touches withEvent:(UIEvent *)event {
    if (!self.touchEnabled) {
        return;
    }
    
    UITouch *aTouch  = [touches anyObject];
    _startPoint = [aTouch locationInView:self];
    NSLog(@"touch in :%f,%f",_startPoint.x,_startPoint.y);
    
    self.shapeLayer = [CAShapeLayer layer];
    self.shapeLayer.lineWidth = 1.0;
    self.shapeLayer.strokeColor = [[UIColor greenColor] CGColor];
    self.shapeLayer.fillColor = [[UIColor clearColor] CGColor];
    self.shapeLayer.lineDashPattern = @[@10, @5];
    [self.layer addSublayer:self.shapeLayer];
    
    // create animation for the layer
    
    CABasicAnimation *dashAnimation;
    dashAnimation = [CABasicAnimation animationWithKeyPath:@"lineDashPhase"];
    [dashAnimation setFromValue:@0.0f];
    [dashAnimation setToValue:@15.0f];
    [dashAnimation setDuration:0.75f];
    [dashAnimation setRepeatCount:HUGE_VALF];
    [self.shapeLayer addAnimation:dashAnimation forKey:@"linePhase"];
}

- (void) touchesMoved:(NSSet<UITouch *> *)touches withEvent:(UIEvent *)event {
    if (!self.touchEnabled) {
        return;
    }
    
    UITouch *aTouch  = [touches anyObject];
    CGPoint point = [aTouch locationInView:self];
    
    // create path for the shape layer
    
    CGMutablePathRef path = CGPathCreateMutable();
    CGPathMoveToPoint(path, NULL, self.startPoint.x, self.startPoint.y);
    CGPathAddLineToPoint(path, NULL, self.startPoint.x, point.y);
    CGPathAddLineToPoint(path, NULL, point.x, point.y);
    CGPathAddLineToPoint(path, NULL, point.x, self.startPoint.y);
    CGPathCloseSubpath(path);
    
    // set the shape layer's path
    
    self.shapeLayer.path = path;
    
    CGPathRelease(path);
}

- (void) touchesEnded:(NSSet<UITouch *> *)touches withEvent:(UIEvent *)event {
    if (!self.touchEnabled) {
        return;
    }
    
    UITouch *aTouch  = [touches anyObject];
    _endPoint = [aTouch locationInView:self];
    NSLog(@"touch end :%f,%f",_endPoint.x,_endPoint.y);
    
    _endPoint.x = MAX(_endPoint.x, 0);
    _endPoint.x = MIN(_endPoint.x, self.bounds.size.width);
    _endPoint.y = MAX(_endPoint.y, 0);
    _endPoint.y = MIN(_endPoint.y, self.bounds.size.height);
    
    self.selectedRect = CGRectMake(MIN(_startPoint.x, _endPoint.x),
                          MIN(_startPoint.y, _endPoint.y),
                          fabs(_startPoint.x - _endPoint.x),
                          fabs(_startPoint.y - _endPoint.y));
    [self.shapeLayer removeFromSuperlayer];
    self.shapeLayer = nil;
    
//    self.selectedRect = [self convertToContentRect:self.selectedRect];
    NSValue * value = [NSValue valueWithCGRect:self.selectedRect];
    [[NSNotificationCenter defaultCenter] postNotificationName:@"TouchDragSelectionDone" object:value];
}

@end
